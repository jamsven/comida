class CreateComponents < ActiveRecord::Migration
  def change
    create_table :components do |t|
      t.string :quantity
      t.references :ingredient
      t.references :division, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
