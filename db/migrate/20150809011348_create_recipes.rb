class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :name
      t.time :duration
      t.integer :difficulty
      t.integer :portions

      t.timestamps null: false
    end
  end
end
