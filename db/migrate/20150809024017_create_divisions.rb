class CreateDivisions < ActiveRecord::Migration
  def change
    create_table :divisions do |t|
      t.string :name
      t.text :instructions

      t.timestamps null: false
    end
  end
end
