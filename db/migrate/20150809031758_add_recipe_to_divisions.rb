class AddRecipeToDivisions < ActiveRecord::Migration
  def change
    add_reference :divisions, :recipe, index: true, foreign_key: true
  end
end
