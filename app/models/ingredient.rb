class Ingredient < ActiveRecord::Base

#attr_accessible :name

  has_many :components
  has_many :divisions, through: :components
end
