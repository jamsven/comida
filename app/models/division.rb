class Division < ActiveRecord::Base
  validates :name, presence:true, length:{maximum: 600}
  validates :instructions, presence:true

  belongs_to :recipe
  has_many :components
  has_many :ingredients, through: :componments

end
