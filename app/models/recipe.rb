class Recipe < ActiveRecord::Base
  has_many :divisions

  validates :name, presence:true, length:{maximum: 600}
  validates :difficulty, presence:true, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 10  }
  validates :portions, presence:true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :duration, presence:true
end
