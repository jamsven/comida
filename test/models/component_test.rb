require 'test_helper'

class ComponentTest < ActiveSupport::TestCase
  def setup
    @component = components(:one) 
  end

  test "component should be valid" do
    assert @component.valid?
  end

  test "quantity should be not empty" do
    @component.quantity = "      "
    assert_not @component.valid?
  end
end
