require 'test_helper'

class IngredientTest < ActiveSupport::TestCase
  def setup
    @ingredient = ingredients(:sugar) 
  end
  test "ingredient should be valid" do
    assert @ingredient.valid?
  end
end
