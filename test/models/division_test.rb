require 'test_helper'

class DivisionTest < ActiveSupport::TestCase

  def setup
    @division = divisions(:dough) 
  end

  test "division should be valid" do
    assert @division.valid?
  end

  test "name should not be empty " do
    @division.name = "    "
    assert_not @division.valid?
  end

  test "name should be max 600 characters long" do
    @division.name = "a" * 601
    assert_not @division.valid?
  end

  test "instructions should not be empty" do
    @division.instructions = "   "
    assert_not @division.valid?
  end
end
