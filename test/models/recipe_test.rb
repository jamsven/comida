require 'test_helper'

class RecipeTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @recipe = recipes(:tasty)
  end

  test "recipe should be valid" do
    assert @recipe.valid?
  end

  test "name should be not empty" do
    @recipe.name = "      "  
    assert_not @recipe.valid?
  end

  test "name should be max 600 characters long" do
    @recipe.name = "a"* 601
    assert_not @recipe.valid?
  end
   
  test "difficulty should be not negative" do
    @recipe.difficulty = -1
    assert_not @recipe.valid?
  end

  test "difficulty should not be bigger than 10" do
    @recipe.difficulty = 11
    assert_not @recipe.valid?
  end

  test "portions should be not negative" do
    @recipe.portions = -1
    assert_not @recipe.valid?
  end
  
  test "duration should be not empty" do
    @recipe.duration = nil 
    assert_not @recipe.valid?
  end
end
